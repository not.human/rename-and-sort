import util
from collections import Counter
import re
import os
import shutil
from distutils.dir_util import copy_tree
import pprint

class Model(object):
	def __init__(self):
		self.listFilenamesWithPath = []
		self.listFilenames = []
		self.listFilenamesWithPath = []

		self.listFilenamesPhotosWithPath = []
		self.listFilenamesPhotosWithoutPath = []
		
		self.nameFileToRename = None

		self.listID = []

		self.dictFilename = {}

		self.listFilenamesWithoutID = []
		self.fileToCopyWithPath = []
		self.fileToCopyID = []
		self.fileToCopy_Path_ID = []

		self.fileToCopyAffichage = []
		self.finalDick = {}
		self.strPop = []
		self.path =""

	def Scan(self):	
		# reinitialize list
		self.listFilenamesWithPath.clear()	
		self.listFilenamesPhotosWithPath.clear()

		self.listFilenamesWithPath = util.getFilenames(self.path)

		for f in self.listFilenamesWithPath:
			myValue = re.search('(.jpg|raw$)', f)
			if(myValue is not None):
				self.listFilenamesPhotosWithPath.append(f)

		self.Parser(self.listFilenamesPhotosWithPath)


	def Copy(self, destination):
		for fsrc in self.fileToCopyWithPath:
			print(fsrc)
			fn = shutil.copy2(fsrc, destination)

	def Parser(self, listFilenamesPhotosWithPath):

		listFilenamesWithoutIDWithoutPath = []
		listFilenamesWithoutIDWithoutPath.clear()
		self.listFilenamesPhotosWithoutPath.clear()
		self.listID.clear()
		self.finalDick.clear()
		self.dictFilename.clear()
		
		nameCounter = Counter()

		for f in listFilenamesPhotosWithPath:
			self.listFilenamesPhotosWithoutPath.append(os.path.basename(f))

		for f in self.listFilenamesPhotosWithoutPath:
			listFilenamesWithoutIDWithoutPath.append(re.sub('(-|_)(\d{4}|\d{5}).jpg', '', f))

		for f in self.listFilenamesPhotosWithoutPath:
			myValue = re.search('(\d{4}|\d{5})', f)
			if(myValue is not None):
				self.listID.append(myValue[0])

		IDWithlistFilenamesWithoutIDWithoutPath = [list(a) for a in zip(listFilenamesWithoutIDWithoutPath, self.listID)]

		zipObj = zip(listFilenamesPhotosWithPath, IDWithlistFilenamesWithoutIDWithoutPath)
		self.dictFilename = dict(zipObj)

		for value in self.dictFilename.values():
			nameCounter[value[0]] += 1	

		self.finalDick = dict(nameCounter) #changer nom de finalDick svp
		self.strPop.clear() #changer nom de strPop stp
		for key, value in self.finalDick.items():
				self.strPop.append(str(value)+" : "+str(key))

	def rename_photos(self, listFilenameToRename, nom):
		for f in listFilenameToRename:

			extension = os.path.splitext(f[0])[1]

			myPath = os.path.dirname(f[0])

			file_name_to_change = f[0]
			file_name_changed = myPath + '\\' + nom + f[1] + extension
			print(file_name_changed)
			os.rename(file_name_to_change, file_name_changed)