import subprocess
import os
from distutils.dir_util import copy_tree
import shutil

def getCurrentDirectory():
		path = os.getcwd()
		return path

def copy_files(source, destination):
	filesname = copy_tree(source, destination, 1, 1, 1, 1)

def getFilenamesWithPath(source):
	listFilenames = []
	for dirpath, dirnames, filenames in os.walk(source):
		for filename in filenames:
			listFilenames.append(os.path.join(dirpath, filename))
	return listFilenames

def getFilenames(source):
	listFilenamesWithPath = []
	for dirpath, dirnames, filenames in os.walk(source):
		for filename in filenames:
			listFilenamesWithPath.append(os.path.join(dirpath, filename))
	return listFilenamesWithPath

def retrievefilename(source):
	return os.listdir(source)

def printfilename(source):
	for filename in os.listdir(source):
		print(filename[1])
	return

def clearScreen():
	tmp = subprocess.call('cls',shell=True)

def changeDir(path):
	os.chdir(path)

def makeDir(dir):

	shutil.rmtree(dir, ignore_errors=True)
	os.mkdir(dir)

def hashNumeral(self, value):
	twoDigits = []
	#faire une liste de pattern de string 01 02 03
	for i in range(1, value):
		if(i < 10):
			twoDigits.append("0"+str(i))
		else:
			twoDigits.append(str(i))
	return twoDigits