from tkinter import *
from tkinter import filedialog
from tkinter import ttk

import randomphotopicker as rpp
import model
import util

class View():
	def __init__(self,parent):
		self.parent=parent
		self.root = Tk()
		self.root.title("Rename-and-sort")
		#self.root.resizable(width=True,height=True)
		self.root.geometry("900x650")
		self.root.iconbitmap("eye.ico")
		self.frameMaster = Frame(self.root, width=400, height=400) #pourquoi mothafuckng wdth adn height is shit ?
		self.frameMenu = Frame(self.root)
		self.frameRandom = Frame(self.root)
		self.path = util.getCurrentDirectory()
		self.pathToCopyTo = util.getCurrentDirectory()
		self.pathRandomSrc = util.getCurrentDirectory()
		self.pathRandomDest = util.getCurrentDirectory()
		self.items = None
		self.init_window()
		self.init_random_window()
		self.init_sort_window()

	def init_window(self):

		self.boutonMaster = Button(self.frameMenu, text='Master',width=15, height=3)
		self.boutonMaster.grid(row=10, column=0, padx=5, pady=200)

		self.boutonRandom = Button(self.frameMenu, text='Random',width=15,height=3)
		self.boutonRandom.grid(row=10,column=1,  pady=200)

	def init_sort_window(self):

		self.pathMasterEntryBox = Entry(self.frameMaster, textvariable='2', width=115)
		self.pathMasterEntryBox.grid(row=2, column=0, columnspan=2, padx=10, pady=10, sticky='w')
		self.pathMasterText = Label(self.frameMaster, text='Path:')
		self.pathMasterText.grid(row=1, column=0,padx=10, sticky='w')

		self.pathMasterButton = Button(self.frameMaster, text='...', width=5, height=1)
		self.pathMasterButton.grid(row=2, column=1, sticky='e')		

		self.pathDestEntryBox = Entry(self.frameMaster, textvariable='3', width=115)
		self.pathDestEntryBox.grid(row=4, column=0, columnspan=2, padx=10, pady=10, sticky='w')
		self.pathDestText = Label(self.frameMaster, text='Dest:')
		self.pathDestText.grid(row=3, column=0,padx=10, sticky='w')

		self.pathCopyButton = Button(self.frameMaster, text='-->', width=5, height=1)
		self.pathCopyButton.grid(row=4, column=1, sticky='e')

		self.scanButton = Button(self.frameMaster, text='Scan', width=8, height=1)
		self.scanButton.grid(row=5	, column=0, padx=10)

		self.copyButton = Button(self.frameMaster, text='Copy', width=16, height=1)
		self.copyButton.grid(row=10	, column=1, padx=10, pady=10, sticky='e')

		self.renameButton = Button(self.frameMaster, text='Rename', width=8)
		self.renameButton.grid(row=5, column=1)
		self.renameEntryBox = Entry(self.frameMaster, textvariable='4', width=60)
		self.renameEntryBox.grid(row=6, column=1, padx=10, pady=10) 

		#Listbox
		self.listbox = Listbox(self.frameMaster, width=60, height=20, activestyle='none',font=("Times",10), selectmode='extended')
		self.listbox.insert(END)
		self.listbox.grid(row=8, column=0, padx=10, sticky='w')

		self.listboxWithNameFile = Listbox(self.frameMaster, width=60, height=20, activestyle='none',font=("Times",10), selectmode='extended')
		self.listboxWithNameFile.insert(END)
		self.listboxWithNameFile.grid(row=8, column=1, padx=10, sticky='w')

		self.extComboBox = ttk.Combobox(self.frameMaster, values=['.jpg?', '.raw?'], width=5, state='readonly')
		self.extComboBox.set('.jpg?')
		self.extComboBox.grid(row=5, column=2, sticky='e')

		self.mainMenuMasterButton = Button(self.frameMaster, text='Main menu', width=20)
		self.mainMenuMasterButton.grid(row=20, column=0, sticky='w', pady=10)

	def init_random_window(self):
		self.mainMenuRandomButton = Button(self.frameRandom, text='Main menu', width=20)
		self.mainMenuRandomButton.grid(row=20, column=0, sticky='w', pady=100)

		self.pathRandomButton = Button(self.frameRandom, text='...', width=5, height=1)
		self.pathRandomButton.grid(row=2, column=2, padx=20, sticky='e')

		self.pathRandomEntryBox = Entry(self.frameRandom, textvariable="5", width=115)
		self.pathRandomEntryBox.grid(row=2, columnspan=2, padx=10, pady=10, sticky='w')
		self.pathRandomText = Label(self.frameRandom, text="Path:")
		self.pathRandomText.grid(row=1, column=0,padx=10, sticky='w')

		self.pathDestEntryBoxRandom = Entry(self.frameRandom, textvariable='6', width=115)
		self.pathDestEntryBoxRandom.grid(row=4, column=0, columnspan=2, padx=10, pady=10, sticky='w')
		self.pathDestTextRandom = Label(self.frameRandom, text='Dest:')
		self.pathDestTextRandom.grid(row=3, column=0,padx=10, sticky='w')

		self.destRandomButton = Button(self.frameRandom, text='-->', width=5, height=1)
		self.destRandomButton.grid(row=4, column=2,padx=20, sticky='e')

		self.nombreRandomEntry = Entry(self.frameRandom, textvariable="7", width=10)
		self.nombreRandomEntry.grid(row=6, column=0,padx=10,pady=10,sticky='w')

		self.nombreRandomText = Label(self.frameRandom, text='Number of photos:')
		self.nombreRandomText.grid(row=5, column=0,padx=10,pady=10,sticky='w')

		self.extDerouleText = Label(self.frameRandom, text="Ext :")
		self.extDerouleText.grid(row=5, column=2, sticky='w', padx=0, pady=10)

		self.extComboBoxRandom = ttk.Combobox(self.frameRandom, values=['.jpg', '.raw'], width=5, state='readonly')
		self.extComboBoxRandom.set('.jpg')
		self.extComboBoxRandom.grid(row=5, column=2, sticky='e')

		self.generateRandomButton = Button(self.frameRandom,text='Generate Random', width=20)
		self.generateRandomButton.grid(row=7, column=2, sticky='e')

	def afficherMaster(self):
		self.frameMaster.pack()
		self.frameMenu.pack_forget()

	def afficherMenu(self):
		self.frameMenu.pack()
		self.frameMaster.pack_forget()
		self.frameRandom.pack_forget()

	def afficherRandom(self):
		self.frameRandom.pack()
		self.frameMenu.pack_forget()

	def open_directory_master(self):
		self.path = filedialog.askdirectory()
		self.pathMasterEntryBox.config(state='normal')
		self.pathMasterEntryBox.delete(0, END)
		self.pathMasterEntryBox.insert(0, self.path)
		self.pathMasterEntryBox.config(state="readonly")

	def open_directory_copy(self):
		self.pathToCopyTo = filedialog.askdirectory()
		self.pathDestEntryBox.config(state='normal')
		self.pathDestEntryBox.delete(0, END)
		self.pathDestEntryBox.insert(0, self.pathToCopyTo)
		self.pathDestEntryBox.config(state="readonly")

	def open_directory_random(self):
		self.pathRandomSrc = filedialog.askdirectory()
		self.pathRandomEntryBox.config(state='normal')
		self.pathRandomEntryBox.delete(0, END)
		self.pathRandomEntryBox.insert(0, self.pathRandomSrc)
		self.pathRandomEntryBox.config(state="readonly")

	def open_directory_destRandom(self):
		self.pathRandomDest = filedialog.askdirectory()
		self.pathDestEntryBoxRandom.config(state='normal')
		self.pathDestEntryBoxRandom.delete(0, END)
		self.pathDestEntryBoxRandom.insert(0, self.pathRandomDest)
		self.pathDestEntryBoxRandom.config(state="readonly")


if __name__ == '__main__':
	app = Controller()