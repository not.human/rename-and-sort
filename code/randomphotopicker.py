import os
import re
import random
from shutil import copyfile

import util
import shutil

class RandomPhotosPicker(object):
	def __init__(self):
		self.listFilenames = []
		self.filenamesPicked = []
		self.numberOfFiles = None
		self.currentDirectory = None
		self.destPath = None
		self.ext = '.jpg'

	def man(self):

		self.listFilenames = self.getFilenamesWithPath(self.currentDirectory)
		self.filenamesPicked  = random.sample(self.listFilenames, self.numberOfFiles)
		self.printfilename(self.filenamesPicked)

		"""
		#Utiliser ceci pour créer dossier si le user donne pas de path
		path = "RandomPhotos" # directory name
		try:
			util.makeDir(self.currentDirectory+path)
			os.mkdir(self.currentDirectory+"/RandomPhotos")
		except OSError:  
			print ("\nCouldn't create \"%s\" because it already exist" % path)
		else:  
		"""

		for item in self.filenamesPicked:
			filename = os.path.basename(item)
			copyfile(item, os.path.join(self.destPath, filename))

	def setNumberOfFile(self, nombre):


		if(nombre == None):
			nombre = 0
		else: 
			self.numberOfFiles = nombre

	def getFilenamesWithPath(self, source):
		listfilenamesWithGoodExt = []
		listFilenames = util.getFilenames(source)

		for f in listFilenames:
			if(f.endswith(self.ext)):
				listfilenamesWithGoodExt.append(f)

		return listfilenamesWithGoodExt

	def getCurrentDirectory(self):
		path = os.getcwd()
		return path

	def printfilename(self, listfilenames):
		for filename in listfilenames:
			print("\""+filename+"\"")