import os 
import re
from collections import Counter
from tkinter import *
from tkinter import filedialog
from tkinter import ttk

import randomphotopicker as rpp
import model
import util
import view

class Controller():
	def __init__(self):
		self.model = model.Model()
		self.vue = view.View(self)
		self.rp = rpp.RandomPhotosPicker()

		#Config/events
		self.vue.listbox.bind('<Double-Button>', self.DoubleClicked)

		self.vue.scanButton.config(command=self.Scan)
		self.vue.copyButton.config(command=self.Copy)
		self.vue.renameButton.config(command=self.Rename)
		self.vue.pathMasterButton.config(command=self.vue.open_directory_master)
		self.vue.pathCopyButton.config(command=self.vue.open_directory_copy)
		self.vue.pathRandomButton.config(command=self.vue.open_directory_random)
		self.vue.destRandomButton.config(command=self.vue.open_directory_destRandom)
		self.vue.boutonMaster.config(command=self.vue.afficherMaster)
		self.vue.boutonRandom.config(command=self.vue.afficherRandom)
		self.vue.mainMenuMasterButton.config(command=self.vue.afficherMenu)
		self.vue.mainMenuRandomButton.config(command=self.vue.afficherMenu)
		self.vue.generateRandomButton.config(command=self.GenerateRandom)

		self.vue.afficherMenu()

		self.vue.root.mainloop()

	def GenerateRandom(self):
		numberOfFile = None

		self.rp.ext = self.vue.extComboBoxRandom.get()

		if(self.vue.nombreRandomEntry.get() is not ''):
			numberOfFile = int(self.vue.nombreRandomEntry.get())
		elif(self.vue.nombreRandomEntry.get() is ''):
			numberOfFile = 0

		self.rp.currentDirectory = self.vue.pathRandomEntryBox.get()
		self.rp.destPath = self.vue.pathDestEntryBoxRandom.get()
		self.rp.setNumberOfFile(numberOfFile)
		self.rp.man()

	def Rename(self):
		self.model.rename_photos(self.model.fileToCopy_Path_ID, self.vue.renameEntryBox.get())
		self.Scan()
		self.vue.clearListBox2()
		self.vue.renameEntryBox.delete(0, END)

	def Scan(self):
		self.vue.clearListBox1()
		self.model.path = self.vue.path
		self.model.Scan()
		self.vue.insertInListbox(self.vue.listbox, self.model.strPop)

	def Copy(self):
		#TODO(if 1st function work change color of item listbox)
		self.model.Copy(self.vue.pathToCopyTo)
		self.vue.listbox.itemconfig(self.vue.items[0], {'bg':'gray79'})

	def DoubleClicked(self, extraArg):
		self.vue.clearListBox2()
		self.model.fileToCopyWithPath.clear()
		self.model.fileToCopyAffichage.clear()

		self.vue.items = None
		self.vue.items = self.vue.listbox.curselection()
		try: 
			self.vue.items = list(map(int, self.vue.items))
		except ValueError:pass

		myStringPattern = re.sub('\d+ : ', '', self.model.strPop[self.vue.items[0]])

		for key,value in self.model.dictFilename.items():
			if(value[0] == myStringPattern):
				self.model.fileToCopyWithPath.append(key)
				self.model.fileToCopyID.append(value[1])

		self.model.fileToCopy_Path_ID = [list(a) for a in zip(self.model.fileToCopyWithPath, self.model.fileToCopyID)]

		#print(self.model.fileToCopy_Path_ID)
		for f in self.model.fileToCopyWithPath:
			self.model.fileToCopyAffichage.append(os.path.basename(f))

		self.model.nameFileToRename = os.path.basename(re.sub('(\d{4}|\d{5}).(jpg|raw)', '', self.model.fileToCopyWithPath[0]))

		self.vue.insertInListbox(self.vue.listboxWithNameFile,self.model.fileToCopyAffichage)

		self.vue.renameEntryBox.config(state='normal')
		self.vue.renameEntryBox.delete(0, END)
		self.vue.renameEntryBox.insert(0, self.model.nameFileToRename)



if __name__ == '__main__':
	app = Controller()